# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 08:29:38 2016

@author: dittaya
"""

#import csv

with open('./PaperList.txt', encoding='utf-8') as f:
    data = [row.strip().split('\t') for row in f]

with open('./PaperList.tex', 'w', encoding='utf-8') as f:
    print('\label{Paper}', file=f)
    for entry in data:
        print('%'+entry[0], file=f)
        print('\procpaper', file=f)
        print('[title={'+entry[2]+'},', file=f)
        print('author={'+entry[3]+'},', file=f)
        print('index={}', file=f)
        print(']{'+entry[1]+'}', file=f)
        print(file=f)